# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config nano curl cmake && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/earthcoinproject/earthcoin.git /opt/earthcoin && \
    cd /opt/earthcoin/src && \
    make -j2 -f makefile.unix

# ---- Release ---- #
FROM ubuntu:14.04 as release 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r earthcoin && useradd -r -m -g earthcoin earthcoin
RUN mkdir /data
RUN chown earthcoin:earthcoin /data
COPY --from=build /opt/earthcoin/src/earthcoind /usr/local/bin/
USER earthcoin
VOLUME /data
EXPOSE 15677 15678

CMD ["/usr/local/bin/earthcoind", "-datadir=/data", "-conf=/data/earthcoin.conf", "-server", "-txindex", "-printtoconsole"]